﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotOrCold
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int number = rnd.Next(0, 100);

            int distinction = 0;
            int customerNumber;

            Console.WriteLine("Игра началась! Введите целое число в диапазоне от 0 до 100: ");

            while (true)
            {
                customerNumber = Convert.ToInt32(Console.ReadLine());
                distinction = Math.Abs(customerNumber - number);

                if (customerNumber == number)
                {
                    Console.WriteLine("А ты молодец, загаданное число: {0} ", number);
                    break;
                }
                else
                {
                    if (distinction <= 5)
                    {
                        // Горячо
                        Console.WriteLine("Воу как горячо, но все же нет, попробуй еще:");
                    }
                    if ((distinction > 5) && (distinction <= 10))
                    {
                        // Тепло
                        Console.WriteLine("Уже определенно теплее, попробуй еще:");
                    }
                    if ((distinction > 10) && (distinction <= 15))
                    {
                        //Холодно
                        Console.WriteLine("Ну как же так, холодо же, попробуй еще:");
                    }
                    if ((distinction > 15))
                    {
                        //   очень холодно
                        Console.WriteLine("Совсем холодно, попробуй еще:");
                    }
                }
            }

            Console.ReadKey();
        }
    }
}
