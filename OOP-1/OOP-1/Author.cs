﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_1
{
    public abstract class Author 
    {
        private String LastName; 
        private String FirstName;
        private String MiddleName;
        private int Phone;
        private String Address;

        public Author(string lastName, string firstName, string middleName, int phone, string address)
        {
            this.LastName = lastName;
            this.FirstName = firstName;
            this.MiddleName = middleName;
            this.Phone = phone;
            this.Address = address;

        }


    }
}
