﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_1
{
    public class Book
    {
        public enum BookType { Fiction, Poetry, Story }

        private String BookName;
        private String Cost;
        private int YearPublish;
        private String Published;
        private String Address;
        private Author Author;

        private BookType type;

        public Book(string bookName, string cost, int yearPublish, string published, string address, BookType type, Author author)
        {
            this.BookName = bookName;
            this.Cost = cost;
            this.YearPublish = yearPublish;
            this.Published = published;
            this.Address = address;
            this.type = type;
            this.Author = author;
        
        }
        
    }
}
