﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_1
{
    public class Publish
    {
        private String Name;
        private String City;
        private String Country;
        private String Published;
        private String Address;

        public Publish(string name, string city, string country, string published, string address)
        {
            this.Name = name;
            this.City = city;
            this.Country = country;
            this.Published = published;
            this.Address = address;
        }
    }
}
