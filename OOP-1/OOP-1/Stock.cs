﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_1
{
    class Stock
    {
        private String Name;
        private String Address;
        private String City;
        private String Region;
        private int Index;

        public Stock(string name, string address, string city, string region, int index)
        {
            this.Name = name;
            this.Address = address;
            this.City = city;
            this.Region = region;
            this.Index = index;
        }
    }
}
