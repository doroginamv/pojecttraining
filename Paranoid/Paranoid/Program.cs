﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paranoid
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Введите Имя задачи: ");
                string name = Console.ReadLine();

                if (String.IsNullOrEmpty(name))
                {
                    throw new System.NullReferenceException("Имя задачи не может быть пустым");
                }


                Console.WriteLine("Введите оценку по времени в часах: ");
                int assessment = Convert.ToInt32(Console.ReadKey());

                if (assessment <= 0 )
                {
                    throw new System.FormatException("Оценка по времени должна быть больше 0:");
                    //throw new System.NullReferenceException("Описание не может быть пустым");
                }
                Console.WriteLine("Введите описание: ");
                string description = Console.ReadLine();

                if (String.IsNullOrEmpty(description))
                {
                    throw new System.NullReferenceException("Описание не может быть пустым");
                }

            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }

            

            Console.ReadKey();
        }

    }
}

//Console.WriteLine("Введите дату начала задачи: ");
//DateTime startDate;
//DateTime.TryParse(Console.ReadLine(), out startDate);

//Console.WriteLine("Введите дату окончания задачи: ");
//DateTime endDate;
//DateTime.TryParse(Console.ReadLine(), out endDate);

//Console.WriteLine("Введите оценку по времени в часах: ");
//int assessment = Convert.ToInt32(Console.ReadKey());